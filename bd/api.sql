/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50088
Source Host           : localhost:3306
Source Database       : api

Target Server Type    : MYSQL
Target Server Version : 50088
File Encoding         : 65001

Date: 2014-08-05 23:53:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `entidad`
-- ----------------------------
DROP TABLE IF EXISTS `entidad`;
CREATE TABLE `entidad` (
  `clave_entidad` int(11) NOT NULL auto_increment,
  `desc_entidad` char(90) default NULL,
  PRIMARY KEY  (`clave_entidad`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of entidad
-- ----------------------------
INSERT INTO `entidad` VALUES ('0', 'Nacional');
INSERT INTO `entidad` VALUES ('2', 'oxaca');
INSERT INTO `entidad` VALUES ('3', 'Guerrero');
INSERT INTO `entidad` VALUES ('4', 'Mexico');

-- ----------------------------
-- Table structure for `indicador`
-- ----------------------------
DROP TABLE IF EXISTS `indicador`;
CREATE TABLE `indicador` (
  `id_indicador` int(11) NOT NULL default '0',
  `indicador` char(160) default NULL,
  `id_tema3` int(11) default NULL,
  `clave_municipio` int(11) default NULL,
  `clave_entidad` int(11) default NULL,
  PRIMARY KEY  (`id_indicador`),
  KEY `indicador_ibfk_3` (`clave_entidad`),
  KEY `indicador_ibfk_1` (`id_tema3`),
  KEY `indicador_ibfk_2` (`clave_municipio`),
  CONSTRAINT `indicador_ibfk_1` FOREIGN KEY (`id_tema3`) REFERENCES `tema3` (`id_tema3`) ON DELETE CASCADE,
  CONSTRAINT `indicador_ibfk_2` FOREIGN KEY (`clave_municipio`) REFERENCES `municipio` (`clave_municipio`) ON DELETE CASCADE,
  CONSTRAINT `indicador_ibfk_3` FOREIGN KEY (`clave_entidad`) REFERENCES `municipio` (`clave_entidad`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoD';

-- ----------------------------
-- Records of indicador
-- ----------------------------
INSERT INTO `indicador` VALUES ('1002000026', 'Nacimientos', '4', '0', '0');
INSERT INTO `indicador` VALUES ('1002000027', 'Nacimientos hombres', '4', '0', '0');
INSERT INTO `indicador` VALUES ('1002000028', 'Nacimientos mujeres', '4', '0', '0');
INSERT INTO `indicador` VALUES ('1002000029', 'Nacimientos de sexo no especificado', '4', '0', '0');
INSERT INTO `indicador` VALUES ('1002000030', 'Defunciones generales', '3', '0', '0');
INSERT INTO `indicador` VALUES ('1002000031', 'Defunciones generales hombres', '3', '0', '0');
INSERT INTO `indicador` VALUES ('1002000032', 'Defunciones generales mujeres', '3', '0', '0');
INSERT INTO `indicador` VALUES ('1002000033', 'Defunciones generales de sexo no especificado', '3', '0', '0');
INSERT INTO `indicador` VALUES ('1002000034', 'Defunciones de menores de un año', '3', '0', '0');
INSERT INTO `indicador` VALUES ('1002000035', 'Defunciones de menores de un año hombres', '3', '0', '0');
INSERT INTO `indicador` VALUES ('1002000036', 'Defunciones de menores de un año mujeres', '3', '0', '0');
INSERT INTO `indicador` VALUES ('1002000037', 'Defunciones de menores de un aÃ±o de sexo no especificado', '3', '0', '0');
INSERT INTO `indicador` VALUES ('1002000038', 'Matrimonios', '2', '0', '0');
INSERT INTO `indicador` VALUES ('1002000039', 'Divorcios', '1', '0', '0');
INSERT INTO `indicador` VALUES ('1002000040', 'indicador', '2', '2', '0');
INSERT INTO `indicador` VALUES ('1002000041', 'indicador', '6', '0', '0');

-- ----------------------------
-- Table structure for `municipio`
-- ----------------------------
DROP TABLE IF EXISTS `municipio`;
CREATE TABLE `municipio` (
  `clave_municipio` int(11) NOT NULL auto_increment,
  `clave_entidad` int(11) NOT NULL default '0',
  `desc_municipio` char(90) default NULL,
  PRIMARY KEY  (`clave_municipio`,`clave_entidad`),
  KEY `clave_municipio` (`clave_municipio`),
  KEY `municipio_ibfk_1` (`clave_entidad`),
  CONSTRAINT `municipio_ibfk_1` FOREIGN KEY (`clave_entidad`) REFERENCES `entidad` (`clave_entidad`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of municipio
-- ----------------------------
INSERT INTO `municipio` VALUES ('0', '0', 'Nacional');
INSERT INTO `municipio` VALUES ('2', '0', 'Oaxca de Juearez');
INSERT INTO `municipio` VALUES ('3', '0', 'Oaxaca de Juarez');

-- ----------------------------
-- Table structure for `tema1`
-- ----------------------------
DROP TABLE IF EXISTS `tema1`;
CREATE TABLE `tema1` (
  `id_tema1` int(11) NOT NULL auto_increment,
  `nombre_tema1` char(90) default NULL,
  PRIMARY KEY  (`id_tema1`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tema1
-- ----------------------------
INSERT INTO `tema1` VALUES ('1', 'Población, Hogares y Vivienda');
INSERT INTO `tema1` VALUES ('2', 'un tema');

-- ----------------------------
-- Table structure for `tema2`
-- ----------------------------
DROP TABLE IF EXISTS `tema2`;
CREATE TABLE `tema2` (
  `id_tema2` int(11) NOT NULL auto_increment,
  `id_tema1` int(11) default NULL,
  `nombre_tema2` char(90) default NULL,
  PRIMARY KEY  (`id_tema2`),
  KEY `tema2_ibfk_1` (`id_tema1`),
  CONSTRAINT `tema2_ibfk_1` FOREIGN KEY (`id_tema1`) REFERENCES `tema1` (`id_tema1`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tema2
-- ----------------------------
INSERT INTO `tema2` VALUES ('1', '1', 'Mortalidad');
INSERT INTO `tema2` VALUES ('2', '1', 'Natalidad y fecundidad');
INSERT INTO `tema2` VALUES ('3', '1', 'Nupcialidad');
INSERT INTO `tema2` VALUES ('4', '2', 'un tema2');

-- ----------------------------
-- Table structure for `tema3`
-- ----------------------------
DROP TABLE IF EXISTS `tema3`;
CREATE TABLE `tema3` (
  `id_tema3` int(11) NOT NULL auto_increment,
  `id_tema2` int(11) default NULL,
  `nombre_tema3` char(90) default NULL,
  PRIMARY KEY  (`id_tema3`),
  KEY `tema3_ibfk_1` (`id_tema2`),
  CONSTRAINT `tema3_ibfk_1` FOREIGN KEY (`id_tema2`) REFERENCES `tema2` (`id_tema2`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tema3
-- ----------------------------
INSERT INTO `tema3` VALUES ('1', '3', 'Divorcios');
INSERT INTO `tema3` VALUES ('2', '3', 'Matrimonios');
INSERT INTO `tema3` VALUES ('3', '1', 'Mortalidad');
INSERT INTO `tema3` VALUES ('4', '2', 'Natalidad');
INSERT INTO `tema3` VALUES ('6', '4', 'un tema3');

-- ----------------------------
-- Table structure for `valor`
-- ----------------------------
DROP TABLE IF EXISTS `valor`;
CREATE TABLE `valor` (
  `id_valor` int(11) NOT NULL auto_increment,
  `anio` int(4) default NULL,
  `valor` double(20,2) default NULL,
  `unidad` char(90) default NULL,
  `id_indicador` int(11) default NULL,
  PRIMARY KEY  (`id_valor`),
  KEY `valor_ibfk_1` (`id_indicador`),
  CONSTRAINT `valor_ibfk_1` FOREIGN KEY (`id_indicador`) REFERENCES `indicador` (`id_indicador`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=270 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of valor
-- ----------------------------
INSERT INTO `valor` VALUES ('1', '1994', '2904381.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('2', '1994', '1462458.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('3', '1994', '1441367.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('4', '1994', '564.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('5', '1994', '419074.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('6', '1994', '237772.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('7', '1994', '181136.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('8', '1994', '166.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('9', '1994', '49305.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('10', '1994', '28038.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('11', '1994', '21192.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('12', '1994', '75.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('13', '1994', '671640.00', '', '1002000038');
INSERT INTO `valor` VALUES ('14', '1994', '35029.00', '', '1002000039');
INSERT INTO `valor` VALUES ('15', '1995', '2750444.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('16', '1995', '1387458.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('17', '1995', '1362547.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('18', '1995', '439.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('19', '1995', '430278.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('20', '1995', '242408.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('21', '1995', '187693.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('22', '1995', '177.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('23', '1995', '48023.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('24', '1995', '27237.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('25', '1995', '20718.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('26', '1995', '68.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('27', '1995', '658114.00', '', '1002000038');
INSERT INTO `valor` VALUES ('28', '1995', '37455.00', '', '1002000039');
INSERT INTO `valor` VALUES ('29', '1996', '2707718.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('30', '1996', '1365863.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('31', '1996', '1341619.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('32', '1996', '236.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('33', '1996', '436321.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('34', '1996', '245017.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('35', '1996', '191168.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('36', '1996', '136.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('37', '1996', '45707.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('38', '1996', '25916.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('39', '1996', '19726.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('40', '1996', '65.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('41', '1996', '670523.00', '', '1002000038');
INSERT INTO `valor` VALUES ('42', '1996', '38545.00', '', '1002000039');
INSERT INTO `valor` VALUES ('43', '1997', '2698425.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('44', '1997', '1359672.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('45', '1997', '1338539.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('46', '1997', '214.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('47', '1997', '440437.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('48', '1997', '247318.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('49', '1997', '192941.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('50', '1997', '178.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('51', '1997', '44377.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('52', '1997', '25165.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('53', '1997', '19145.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('54', '1997', '67.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('55', '1997', '707840.00', '', '1002000038');
INSERT INTO `valor` VALUES ('56', '1997', '40792.00', '', '1002000039');
INSERT INTO `valor` VALUES ('57', '1998', '2668428.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('58', '1998', '1345837.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('59', '1998', '1322244.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('60', '1998', '347.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('61', '1998', '444665.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('62', '1998', '249030.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('63', '1998', '195460.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('64', '1998', '175.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('65', '1998', '42183.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('66', '1998', '23557.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('67', '1998', '18548.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('68', '1998', '78.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('69', '1998', '704456.00', '', '1002000038');
INSERT INTO `valor` VALUES ('70', '1998', '45889.00', '', '1002000039');
INSERT INTO `valor` VALUES ('71', '1999', '2769089.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('72', '1999', '1384810.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('73', '1999', '1384000.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('74', '1999', '279.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('75', '1999', '443950.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('76', '1999', '247833.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('77', '1999', '195979.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('78', '1999', '138.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('79', '1999', '40283.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('80', '1999', '22754.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('81', '1999', '17463.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('82', '1999', '66.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('83', '1999', '743856.00', '', '1002000038');
INSERT INTO `valor` VALUES ('84', '1999', '49271.00', '', '1002000039');
INSERT INTO `valor` VALUES ('85', '2000', '2798339.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('86', '2000', '1398877.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('87', '2000', '1398703.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('88', '2000', '759.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('89', '2000', '437667.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('90', '2000', '244302.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('91', '2000', '193253.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('92', '2000', '112.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('93', '2000', '38621.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('94', '2000', '21793.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('95', '2000', '16769.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('96', '2000', '59.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('97', '2000', '707422.00', '', '1002000038');
INSERT INTO `valor` VALUES ('98', '2000', '52358.00', '', '1002000039');
INSERT INTO `valor` VALUES ('99', '2001', '2767610.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('100', '2001', '1390066.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('101', '2001', '1377151.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('102', '2001', '393.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('103', '2001', '443127.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('104', '2001', '245998.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('105', '2001', '196789.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('106', '2001', '340.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('107', '2001', '35911.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('108', '2001', '20302.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('109', '2001', '15487.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('110', '2001', '122.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('111', '2001', '665434.00', '', '1002000038');
INSERT INTO `valor` VALUES ('112', '2001', '57370.00', '', '1002000039');
INSERT INTO `valor` VALUES ('113', '2002', '2699084.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('114', '2002', '1345504.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('115', '2002', '1350142.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('116', '2002', '3438.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('117', '2002', '459687.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('118', '2002', '255522.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('119', '2002', '203846.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('120', '2002', '319.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('121', '2002', '36567.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('122', '2002', '20734.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('123', '2002', '15690.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('124', '2002', '143.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('125', '2002', '616654.00', '', '1002000038');
INSERT INTO `valor` VALUES ('126', '2002', '60641.00', '', '1002000039');
INSERT INTO `valor` VALUES ('127', '2003', '2655894.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('128', '2003', '1307080.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('129', '2003', '1348354.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('130', '2003', '460.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('131', '2003', '472140.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('132', '2003', '261680.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('133', '2003', '210096.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('134', '2003', '364.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('135', '2003', '33355.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('136', '2003', '19008.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('137', '2003', '14236.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('138', '2003', '111.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('139', '2003', '584142.00', '', '1002000038');
INSERT INTO `valor` VALUES ('140', '2003', '64248.00', '', '1002000039');
INSERT INTO `valor` VALUES ('141', '2004', '2625056.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('142', '2004', '1302411.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('143', '2004', '1322074.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('144', '2004', '571.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('145', '2004', '473417.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('146', '2004', '261919.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('147', '2004', '211294.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('148', '2004', '204.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('149', '2004', '32764.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('150', '2004', '18524.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('151', '2004', '14163.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('152', '2004', '77.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('153', '2004', '600563.00', '', '1002000038');
INSERT INTO `valor` VALUES ('154', '2004', '67575.00', '', '1002000039');
INSERT INTO `valor` VALUES ('155', '2005', '2567906.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('156', '2005', '1284304.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('157', '2005', '1283009.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('158', '2005', '593.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('159', '2005', '495240.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('160', '2005', '273126.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('161', '2005', '221968.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('162', '2005', '146.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('163', '2005', '32603.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('164', '2005', '18214.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('165', '2005', '14318.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('166', '2005', '71.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('167', '2005', '595713.00', '', '1002000038');
INSERT INTO `valor` VALUES ('168', '2005', '70184.00', '', '1002000039');
INSERT INTO `valor` VALUES ('169', '2006', '2505939.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('170', '2006', '1254600.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('171', '2006', '1250937.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('172', '2006', '402.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('173', '2006', '494471.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('174', '2006', '274091.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('175', '2006', '220240.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('176', '2006', '140.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('177', '2006', '30899.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('178', '2006', '17373.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('179', '2006', '13447.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('180', '2006', '79.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('181', '2006', '586978.00', '', '1002000038');
INSERT INTO `valor` VALUES ('182', '2006', '72396.00', '', '1002000039');
INSERT INTO `valor` VALUES ('183', '2007', '2655083.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('184', '2007', '1330390.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('185', '2007', '1324087.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('186', '2007', '606.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('187', '2007', '514420.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('188', '2007', '284910.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('189', '2007', '229336.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('190', '2007', '174.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('191', '2007', '30425.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('192', '2007', '17190.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('193', '2007', '13142.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('194', '2007', '93.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('195', '2007', '595209.00', '', '1002000038');
INSERT INTO `valor` VALUES ('196', '2007', '77255.00', '', '1002000039');
INSERT INTO `valor` VALUES ('197', '2008', '2636110.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('198', '2008', '1320177.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('199', '2008', '1315435.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('200', '2008', '498.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('201', '2008', '539530.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('202', '2008', '300837.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('203', '2008', '238523.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('204', '2008', '170.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('205', '2008', '29537.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('206', '2008', '16585.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('207', '2008', '12851.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('208', '2008', '101.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('209', '2008', '589352.00', '', '1002000038');
INSERT INTO `valor` VALUES ('210', '2008', '81851.00', '', '1002000039');
INSERT INTO `valor` VALUES ('211', '2009', '2577214.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('212', '2009', '1296770.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('213', '2009', '1279883.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('214', '2009', '561.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('215', '2009', '564673.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('216', '2009', '316058.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('217', '2009', '248371.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('218', '2009', '244.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('219', '2009', '28988.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('220', '2009', '16231.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('221', '2009', '12697.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('222', '2009', '60.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('223', '2009', '558913.00', '', '1002000038');
INSERT INTO `valor` VALUES ('224', '2009', '84302.00', '', '1002000039');
INSERT INTO `valor` VALUES ('225', '2010', '2643908.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('226', '2010', '1326612.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('227', '2010', '1317023.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('228', '2010', '273.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('229', '2010', '592018.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('230', '2010', '332027.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('231', '2010', '259669.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('232', '2010', '322.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('233', '2010', '28865.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('234', '2010', '16148.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('235', '2010', '12637.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('236', '2010', '80.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('237', '2010', '568632.00', '', '1002000038');
INSERT INTO `valor` VALUES ('238', '2010', '86042.00', '', '1002000039');
INSERT INTO `valor` VALUES ('239', '2011', '2586287.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('240', '2011', '1300026.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('241', '2011', '1285962.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('242', '2011', '299.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('243', '2011', '590693.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('244', '2011', '332646.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('245', '2011', '257468.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('246', '2011', '579.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('247', '2011', '29050.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('248', '2011', '16171.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('249', '2011', '12773.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('250', '2011', '106.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('251', '2011', '570954.00', '', '1002000038');
INSERT INTO `valor` VALUES ('252', '2011', '91285.00', '', '1002000039');
INSERT INTO `valor` VALUES ('253', '2012', '2498880.00', 'Nacimientos', '1002000026');
INSERT INTO `valor` VALUES ('254', '2012', '1262938.00', 'Nacimientos', '1002000027');
INSERT INTO `valor` VALUES ('255', '2012', '1235719.00', 'Nacimientos', '1002000028');
INSERT INTO `valor` VALUES ('256', '2012', '223.00', 'Nacimientos', '1002000029');
INSERT INTO `valor` VALUES ('257', '2012', '602354.00', 'Defunciones', '1002000030');
INSERT INTO `valor` VALUES ('258', '2012', '338377.00', 'Defunciones', '1002000031');
INSERT INTO `valor` VALUES ('259', '2012', '263440.00', 'Defunciones', '1002000032');
INSERT INTO `valor` VALUES ('260', '2012', '537.00', 'Defunciones', '1002000033');
INSERT INTO `valor` VALUES ('261', '2012', '28956.00', 'Defunciones', '1002000034');
INSERT INTO `valor` VALUES ('262', '2012', '16151.00', 'Defunciones', '1002000035');
INSERT INTO `valor` VALUES ('263', '2012', '12671.00', 'Defunciones', '1002000036');
INSERT INTO `valor` VALUES ('264', '2012', '134.00', 'Defunciones', '1002000037');
INSERT INTO `valor` VALUES ('265', '2012', '585434.00', '', '1002000038');
INSERT INTO `valor` VALUES ('266', '2012', '99509.00', '', '1002000039');
INSERT INTO `valor` VALUES ('267', '1994', '20000.00', 'Ninguna', '1002000040');
INSERT INTO `valor` VALUES ('268', '1994', '2000.00', 'unidad', '1002000041');
INSERT INTO `valor` VALUES ('269', '1994', '2000.00', 'unidad', '1002000041');
