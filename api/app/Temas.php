<?php

$app->get('/temas1', function() use($app_request, $app_response) {
            $xml = formatos($app_request);
            conect();
            $datos = getTemasNivel1(fields($app_request, "tema1"), filter($app_request), order_by($app_request), limit($app_request));
            $tem = "Tema_Nivel_1";
            $status = 404;
            if ($datos) {
                $status = 200;
            } else {
                $datos = array();
            }
            respuesta($app_response, $status, $tem, $datos, $xml);
        });

$app->get('/temas1/:id_tema1', function($id_tema1) use($app_request, $app_response) {
            $xml = formatos($app_request);
            conect();
            $datos = getTemaNivel1($id_tema1, fields($app_request, "tema1"));

            $status = 404;
            $tem = "Tema_Nivel_1";
            if ($datos) {
                $status = 200;
            } else {
                $datos = array();
            }
            respuesta($app_response, $status, $tem, $datos, $xml);
        });

$app->get('/temas1/:id_tema1/temas2', function($id_tema1) use($app_request, $app_response) {
            $xml = formatos($app_request);
            conect();
            $datos = getTemasNivel2($id_tema1, fields($app_request, "tema2"), filter($app_request), order_by($app_request), limit($app_request));

            $status = 404;
            $tem = "Tema_Nivel_2";
            if ($datos) {
                $tem = "Tema_Nivel_2";

                $status = 200;
            } else {
                $datos = array();
            }
            respuesta($app_response, $status, $tem, $datos, $xml);
        });

$app->get('/temas1/:id_tema1/temas2/:id_tema2', function($id_tema1, $id_tema2) use($app_request, $app_response) {
            $xml = formatos($app_request);
            conect();
            $datos = getTemaNivel2($id_tema2, fields($app_request, "tema2"));
            $tem = "Tema_Nivel_2";
            $status = 404;
            if ($datos) {

                $status = 200;
            } else {
                $datos = array();
            }
            respuesta($app_response, $status, $tem, $datos, $xml);
        });

$app->get('/temas1/:id_tema1/temas2/:id_tema2/temas3', function($id_tema1, $id_tema2) use($app_request, $app_response) {
            $xml = formatos($app_request);
            conect();
            $datos = getTemasNivel3($id_tema2, fields($app_request, "temas3"), filter($app_request), order_by($app_request), limit($app_request));
            $tem = "Tema_Nivel_3";
            $status = 404;
            if ($datos) {

                $status = 200;
            } else {
                $datos = array();
            }
            respuesta($app_response, $status, $tem, $datos, $xml);
        });

$app->get('/temas1/:id_tema1/temas2/:id_tema2/temas3/:id_tema3', function($id_tema1, $id_tema2, $id_tema3) use($app_request, $app_response) {
            $xml = formatos($app_request);
            conect();
            $datos = getTemaNivel3($id_tema3, fields($app_request, "tema3"));
            $tem = "Tema_Nivel_3";
            $status = 404;
            if ($datos) {
                $status = 200;
            } else {
                $datos = array();
            }
            respuesta($app_response, $status, $tem, $datos, $xml);
        });

$app->get('/temas1/:id_tema1/temas2/:id_tema2/temas3/:id_tema3/indicadores', function($id_tema1, $id_tema2, $id_tema3) use($app_request, $app_response) {
            $xml = formatos($app_request);
            conect();
            $datos = getInds($id_tema3, fields($app_request, "indicador"), filter($app_request), order_by($app_request), limit($app_request));
            $tem = "Indicador";
            $status = 404;
            if ($datos) {
                $status = 200;
            } else {
                $datos = array();
            }
            respuesta($app_response, $status, $tem, $datos, $xml);
        });

$app->get('/temas1/:id_tema1/temas2/:id_tema2/temas3/:id_tema3/indicadores/:id', function($id_tema1, $id_tema2, $id_tema3, $id) use($app_request, $app_response) {
            $xml = formatos($app_request);
            conect();
            $datos = getInd($id_tema3, $id, 0, fields($app_request, "indicador inner join valor"), filter($app_request), order_by($app_request), limit($app_request));
            $tem = "Indicador";
            $status = 404;
            if ($datos) {
                $status = 200;
            } else {
                $datos = array();
            }
            respuesta($app_response, $status, $tem, $datos, $xml);
        });

$app->get('/temas1/:id_tema1/temas2/:id_tema2/temas3/:id_tema3/indicadores/:id/:anio', function($id_tema1, $id_tema2, $id_tema3, $id, $anio) use($app_request, $app_response) {
            $xml = formatos($app_request);
            conect();
            $datos = getInd($id_tema3, $id, $anio, fields($app_request, "indicador inner join valor"));
            $tem = "Indicador";
            $status = 404;
            if ($datos) {
                $status = 200;
            } else {
                $datos = array();
            }
            respuesta($app_response, $status, $tem, $datos, $xml);
        });