<?php

function isIdEntidad($num, $tipo = "Entidad") {
    if (!is_numeric($num)) {
        $GLOBALS['response']->header("accept", "El id debe ser numerico");
	
        return "  ";
    }
    return true;
}

function formatos($app_request, $app_response = null) {
    $xml = $app_request->headers("accept");
    if ($xml == "JSON" || $xml == "XML" || $xml == "json" || $xml == "xml") {
        return $xml;
    } else if ($xml != null) {
        $app_response->header("accept-error", "formatos validos: json,xml");
    }
    return "json";
}

function fields($app_request, $tabla) {
    $r = $app_request->get("fields");
    if ($r) {
        $campos = explode(",", $r);
        $campos_real = camposTabla($tabla);
        $result = "";
        $msj = "";
        foreach ($campos as $key => $value) {
            if (is_numeric(array_search($value, $campos_real))) {
                $result.=$campos[$key] . ",";
            } else {
                $msj.="el campo $value no se encontró ";
            }
        }if ($result == "") {
            $GLOBALS['response']->header("accept", "$msj");
            return "*";
        } else if ($msj != "") {
            $GLOBALS['response']->header("accept", "$msj");
        }
        return substr($result, 0, sizeof(str_split($result)) - 1);
    }
    return "*";
}

function filter($app_request) {
    $campo = $app_request->get("filter_by");
    $valor = null;
    if ($campo) {
        $valor = $app_request->get("$campo");
        if (!$valor) {
            $GLOBALS['response']->header("accept", "falta especificar el campo a filtrar");
        }
    }
    if (!(is_null($valor) && is_null($campo))) {
        return " where $campo='$valor' ";
    }
    return "";
}

function order_by($app_request) {
    $r = $app_request->get("order_by");
    $asc = $app_request->get("asc");
    $desc = $app_request->get("desc");
    if ($r && ($asc || $desc)) {
        if ($asc)
            return " order by $r  asc";
        else
            return " order by $r  desc";
    }
    return "";
}

function limit($app_request) {
    $limit = $app_request->get("limit");
    if (is_numeric($limit)) {
        return " limit $limit";
    } else if ($limit) {
        $GLOBALS['response']->header("accept", "limit debe ser un número");
    }
    return "";
}

?>
