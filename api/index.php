<?php
require 'app/vendor/autoload.php';

/* How to organize a large Slim Framework application
 * http://www.slimframework.com/news/how-to-organize-a-large-slim-framework-application
 */

/*
 * Documentation:
 * http://php.net/manual/en/function.date-default-timezone-set.php
 * http://www.php.net/manual/en/timezones.php
 */

$app = new \Slim\Slim(array(
    'mode' => 'development'
));

$app_response = $app->response();
$app_request = $app->request();
$app_request->headers("Access-Control-Allow-Origin","*");
$GLOBALS['response']=$app_response;
require_once './app/lib/DB.php';
require_once './app/lib/response.php';
require_once './app/lib/Validador.php';
require_once 'app/Entidades.php';
require_once 'app/Temas.php';
require_once 'app/post.php';
require_once 'app/metodoPut.php';
require_once 'app/metodoDelete.php';
$app->run();


?>